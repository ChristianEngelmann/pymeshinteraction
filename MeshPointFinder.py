#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Class which contains functionalities to find points within a given mesh
according to given criteria.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""

import vtk
import numpy as np


class MeshPointFinder:
    def __init__(self, mesh):
        self.mesh = mesh

    def findClosestPoint(self, point):
        Loc = vtk.vtkPointLocator()
        Loc.SetDataSet(self.mesh)

        Id = Loc.FindClosestPoint(point)
        return Id, self.mesh.GetPoints().GetPoint(Id)

    def findPointsWithinRadius(self, point, radius, dx, dy):
        Loc = vtk.vtkPointLocator()
        Loc.SetDataSet(self.mesh)
        points = []
        ids = []
        iDs = vtk.vtkIdList()
        Loc.FindPointsWithinRadius(radius, point, iDs)
        for i in range(iDs.GetNumberOfIds()):
            points.append(self.mesh.GetPoints().GetPoint(iDs.GetId(i)))
            bulk_node_ids = self.mesh.GetPointData().GetArray("bulk_node_ids")
            ids.append(int(bulk_node_ids.GetTuple(iDs.GetId(i))[0]))
        if len(points) < 3:
            points = []
            ids = []
            iDs = vtk.vtkIdList()
            Loc.FindClosestNPoints(3, point, iDs)
            for i in range(iDs.GetNumberOfIds()):
                points.append(self.mesh.GetPoints().GetPoint(iDs.GetId(i)))
                bulk_node_ids = self.mesh.GetPointData().GetArray(
                        "bulk_node_ids")
                ids.append(int(bulk_node_ids.GetTuple(iDs.GetId(i))[0]))
        return points, ids

    def findPointsOnPlane(self, nx, ny, nz, dx, dy, dz, ox, oy, oz):
        Loc = vtk.vtkPointLocator()
        Loc.SetDataSet(self.mesh)
        points = []
        ids = []
        for nx_i in range(nx):
            for ny_i in range(ny):
                for nz_i in range(nz):
                    point = np.array(
                            [ox + nx_i * dx, oy + ny_i * dy, oz+nz_i * dz])
                    Id = Loc.FindClosestPoint(point)
                    points.append(self.mesh.GetPoints().GetPoint(Id))
                    ids.append(Id)
        return points, ids

    def extractListNegative(self, pointlist):
        meshpoints = self.mesh.GetPoints()
        nmeshpoints = meshpoints.GetNumberOfPoints()
        iDs = []
        ids = []
        for point in pointlist:
            iDs.append(point[0])
        points = []
        for iD in range(nmeshpoints):
            if(iD in iDs):
                iDs.remove(iD)
            elif(meshpoints.GetPoint(iD)[2] == 0):
                points.append(meshpoints.GetPoint(iD))
                ids. append(iD)
        return points
