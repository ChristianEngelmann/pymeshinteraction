#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Main tool to create and edit vtu grid files. Here, two and three dimensional
rectangular grids can be created. Additionally regular grids can be filled with
cells and delaunay is available for 2 and 3 dimensional grids. Build in
functions to create and remove heteregenous property vectors are implemented as
well. Additionally a function combining multiple 2 dimensional grids using del-
aunay to generate the new cell distribution is implemented.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""

import vtk
import numpy as np


class MeshInteractor:
    def __init__(self, meshName):
        self.meshName = meshName

    def create3DRectangularGrid(
            self, extension_points_X, originX, lengthX, extension_points_Y,
            originY, lengthY, extension_points_Z, originZ, lengthZ):
        self.extension_points_X = extension_points_X
        self.extension_points_Y = extension_points_Y
        self.extension_points_Z = extension_points_Z
        self.lengthX = lengthX
        self.lengthY = lengthY
        self.lengthZ = lengthZ
        self.ox = originX
        self.oy = originY
        self.oz = originZ
        self.celltypes = []
        self.grid = vtk.vtkUnstructuredGrid()

        self.points = vtk.vtkPoints()
        self.cells = vtk.vtkCellArray()
        if self.extension_points_X == 1:
            self.dx = 0
        else:
            self.dx = self.lengthX/float(self.extension_points_X-1)
        if self.extension_points_Y == 1:
            self.dy = 0
        else:
            self.dy = self.lengthY/float(self.extension_points_Y-1)
        if self.extension_points_Z == 1:
            self.dz = 0
        else:
            self.dz = self.lengthZ/float(self.extension_points_Z-1)
        for pointIdZ in range(self.extension_points_Z):
            for pointIdY in range(self.extension_points_Y):
                for pointIdX in range(self.extension_points_X):
                    coordx = pointIdX * self.dx
                    coordy = pointIdY * self.dy
                    coordz = pointIdZ * self.dz - self.lengthZ
                    self.points.InsertNextPoint(coordx, coordy, coordz)
        self.grid.SetPoints(self.points)

    def fillRectangularGridWithVoxels(self):
        self.celltypes = []
        for pointIdZ in range(self.extension_points_Z):
            for pointIdY in range(self.extension_points_Y):
                for pointIdX in range(self.extension_points_X):
                    bedX = int((pointIdX % self.extension_points_X + 1) /
                               self.extension_points_X)
                    bedY = int((pointIdY % self.extension_points_Y + 1) /
                               self.extension_points_Y)
                    bedZ = int((pointIdZ % self.extension_points_Z + 1) /
                               self.extension_points_Z)
                    if(bedX == 0 and bedY == 0 and bedZ == 0):
                        cell = vtk.vtkVoxel()
                        Ids = cell.GetPointIds()
                        for kId in range(8):
                            nodeId = pointIdX + pointIdY * \
                                self.extension_points_X + pointIdZ * \
                                (self.extension_points_X *
                                 self.extension_points_Y)
                            nodeId += kId % 2
                            nodeId += int(kId / 2) % 2 * \
                                self.extension_points_X
                            nodeId += int(kId/4) * self.extension_points_X * \
                                self.extension_points_Y
                            Ids.SetId(kId, nodeId)
                        self.cells.InsertNextCell(cell)
                        self.celltypes.append(11)
        self.grid.SetCells(np.array(self.celltypes), self.cells)

    def fillRectangularPlanarGridWithQuads(self):
        self.celltypes = []

        def nodeIdFinder(id1, extension1, id2, extension2):
            bed1 = int((id1 % extension1 + 1) / extension1) == 0
            bed2 = int((id2 % extension2 + 1)/extension2) == 0
            if(bed1 and bed2):
                cell = vtk.vtkQuad()
                Ids = cell.GetPointIds()
                for kId in range(4):
                    nodeId = id1 + id2 * (extension1)
                    nodeId += kId % 2
                    nodeId += int(kId / 2) * (extension1 + 1 - 2 * (kId % 2))
                    Ids.SetId(kId, nodeId)
                return cell
            else:
                return 0

        for pointIdZ in range(self.extension_points_Z):
            for pointIdY in range(self.extension_points_Y):
                for pointIdX in range(self.extension_points_X):

                    nox = self.extension_points_X == 1
                    noy = self.extension_points_Y == 1
                    noz = self.extension_points_Z == 1
                    cell = 0
                    if noz:
                            cell = nodeIdFinder(
                                    pointIdX, self.extension_points_X,
                                    pointIdY, self.extension_points_Y)
                    if noy:
                            cell = nodeIdFinder(
                                    pointIdX, self.extension_points_X,
                                    pointIdZ, self.extension_points_Z)
                    if nox:
                            cell = nodeIdFinder(
                                    pointIdY, self.extension_points_Y,
                                    pointIdZ, self.extension_points_Z)
                    if cell != 0:
                        self.cells.InsertNextCell(cell)
                        self.celltypes.append(9)
        self.grid.SetCells(np.array(self.celltypes), self.cells)

    def CombineMultiple2DGrids(self, gridlist):
        mainGrid = vtk.vtkUnstructuredGrid()

        appendFilter = vtk.vtkAppendFilter()
        appendFilter.AddInputData(self.grid)
        for i in range(len(gridlist)):
            appendFilter.AddInputData(gridlist[i])
        appendFilter.Update()
        newGrid = vtk.vtkUnstructuredGrid()
        newGrid.ShallowCopy(appendFilter.GetOutput())
        cellmerge = vtk.vtkMergeCells()
        cellmerge.MergeDuplicatePointsOn()
        cellmerge.SetTotalNumberOfCells(newGrid.GetNumberOfCells())
        cellmerge.SetTotalNumberOfPoints(newGrid.GetNumberOfPoints())
        cellmerge.SetTotalNumberOfDataSets(1)
        cellmerge.SetPointMergeTolerance(1e-7)
        cellmerge.SetUnstructuredGrid(mainGrid)
        cellmerge.MergeDataSet(newGrid)
        cellmerge.Finish()
        self.grid = cellmerge.GetUnstructuredGrid()
        self.grid.GetCells().Initialize()
        self.CreateMultipleTriangles()

    def CreateMeshFromPoints(self, points):
        self.grid = vtk.vtkUnstructuredGrid()
        self.points = vtk.vtkPoints()
        propertyvector = vtk.vtkDataArray.CreateDataArray(
                vtk.VTK_UNSIGNED_LONG)
        propertyvector.SetName("bulk_node_ids")
        for i in range(len(points[0])):
            point = points[0][i]
            iD = points[1][i]
            self.points.InsertNextPoint(point)
            propertyvector.InsertNextTuple1(iD)
        self.grid.SetPoints(self.points)
        self.grid.GetPointData().AddArray(propertyvector)

    def CreateMultipleTriangles(self):
        if(self.grid.GetNumberOfPoints() == 1):
            cells = vtk.vtkCellArray()
            cell = vtk.vtkVertex()
            Ids = cell.GetPointIds()
            Ids.SetId(0, 0)
            cells.InsertNextCell(cell)
            self.grid.SetCells(1, cells)
        if(self.grid.GetNumberOfPoints() == 2):
            cells = vtk.vtkCellArray()
            cell = vtk.vtkLine()
            Ids = cell.GetPointIds()
            for kId in range(2):
                Ids.SetId(kId, kId)
            cells.InsertNextCell(cell)
            self.grid.SetCells(3, cells)
        else:
            delaunay = vtk.vtkDelaunay2D()
            delaunay.SetInputData(self.grid)
            delaunay.Update()
            cells = delaunay.GetOutput().GetPolys()

            self.grid.SetCells(5, cells)

    def getMeshAreaForTriangles(self):
        cells = self.grid.GetCells()
        points = self.grid.GetPoints()
        total_area = 0
        pts = vtk.vtkIdList()
        cells.InitTraversal()
        for i in range(cells.GetNumberOfCells()):
            area = 0
            cells.GetNextCell(pts)
            id0 = pts.GetId(0)
            id1 = pts.GetId(1)
            id2 = pts.GetId(2)
            p0 = np.array(points.GetPoint(id0))
            p1 = np.array(points.GetPoint(id1))
            p2 = np.array(points.GetPoint(id2))

            v1 = p1-p0
            v2 = p2-p0
            area = np.abs(v1[0]*v2[1]-v1[1]*v2[0])/2.

            total_area += area
        return total_area

    def create3DTriangles(self):
        delaunay = vtk.vtkDelaunay3D()
        delaunay.SetInputData(self.grid)
        delaunay.Update()
        cells = delaunay.GetOutput().GetCells()
        self.grid.SetCells(10, cells)

    def setDxDyDz(self, dx, dy, dz):
        self.dx, self.dy, self.dz = dx, dy, dz

    def AddPropertyVector(self, values, name, tYpe):
        values = values * np.ones(self.grid.GetPoints().GetNumberOfPoints())
        if tYpe == "double":
            propertyvector = vtk.vtkDoubleArray()
        if tYpe == "unsigned_long":
            propertyvector = vtk.vtkDataArray.CreateDataArray(
                    vtk.VTK_UNSIGNED_LONG)
            values = np.array(
                    values, dtype=np.unsignedinteger)
        propertyvector.SetName(name)
        for value in values:
            propertyvector.InsertNextTuple1(value)

        self.grid.GetPointData().AddArray(propertyvector)

    def RemovePropertyVector(self, name):
        self.grid.GetPointData().RemoveArray(name)

    def OutputMesh(self, outputdir):
        writer = vtk.vtkXMLUnstructuredGridWriter()
        writer.SetFileName(outputdir + self.meshName + ".vtu")
        writer.SetInputData(self.grid)
        writer.Write()
